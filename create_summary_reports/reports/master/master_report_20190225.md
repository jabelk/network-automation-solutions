| Hostname | Platform Type | Memory Free | OS Version |
|--|--|--|--|
| eos-leaf1 | vEOS | 3142 | 4.20.0F-7058194.bloomingtonrel (engineering build) |
| eos-leaf2 | vEOS | 3143 | 4.20.0F-7058194.bloomingtonrel (engineering build) |
| eos-spine1 | vEOS | 3142 | 4.20.0F-7058194.bloomingtonrel (engineering build) |
| eos-spine2 | vEOS | 3142 | 4.20.0F-7058194.bloomingtonrel (engineering build) |
| csr1 | CSR1000V | 6294980 | 16.06.02 |
| csr1 | CSR1000V | 1861224 | 16.06.02 |
| csr2 | CSR1000V | 6246676 | 16.06.02 |
| csr2 | CSR1000V | 1857901 | 16.06.02 |
| csr3 | CSR1000V | 6283456 | 16.06.02 |
| csr3 | CSR1000V | 1857823 | 16.06.02 |
| vmx7 | vmx | 3092200 | 15.1F4.15 |
| vmx8 | vmx | 3091368 | 15.1F4.15 |
| vmx9 | vmx | 1088484 | 15.1F4.15 |
| nxos-spine1 | NX-OSv Chassis | 1870 | 7.3(1)D1(1) [build 7.3(1)D1(0.10)] |
| nxos-spine2 | NX-OSv Chassis | 1870 | 7.3(1)D1(1) [build 7.3(1)D1(0.10)] |
