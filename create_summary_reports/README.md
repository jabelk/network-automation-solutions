# Easy Wins: Create Summary Reports

The goal of this lab is to **collect information** using a chosen transport medium (CLI, SNMP, REST, etc.)
and **store the information** into text files, and create a summary report based on the information. 

This will require the following:
1. Build inventory file, host/group vars and at least one playbook to gather the data, parse it and write to files.
2. Write one or many J2 files which format the parsed output into readable summaries
3. Plug in the variables from the device into the J2 files to render the reports.

Enabled Fact Caching to Speed up Delivery:
```
[defaults]
gathering = smart
fact_caching = jsonfile
fact_caching_connection = /tmp/facts_cache

# two hours timeout
fact_caching_timeout = 7200
```
source: [https://stackoverflow.com/questions/32703874/fastest-way-to-gather-facts-to-fact-cache]