# Network Automation Solutions

## Lab Topology

Hosted on public cloud (ravello), with both public IP's (not reserved/elastic) 
and local IP which jump host can use every time, resolved by the jump host `/etc/hosts` file

- 3 CSR1000v
    - CSR1
    - CSR2
    - CSR3
- 3 VMX Junos
    - vmx7
    - vmx8
    - vmx9
- 2 NX-OS devices
    - nxos-spine1
    - nxos-spine2
- 4 EOS vEOS 
    - eos-spine1
    - eos-spine2
    - eos-leaf1
    - eos-leaf2
- 1 Ubuntu 16.04 jump host

`/etc/hosts`

```
10.0.0.11   eos-spine1
10.0.0.12   eos-spine2
10.0.0.21   eos-leaf1
10.0.0.22   eos-leaf2
10.0.0.37   vmx7  
10.0.0.38   vmx8  
10.0.0.39   vmx9  
10.0.0.51   csr1
10.0.0.52   csr2
10.0.0.53   csr3
10.0.0.71   nxos-spine1
10.0.0.72   nxos-spine2

```